package org.erachain.dapp;

public interface DAppTimed {
    boolean processByTime();

    void orphanByTime();

}
